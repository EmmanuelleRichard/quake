require 'spec_helper'
require 'quakeclass'
require 'json'

describe 'Quakeclass' do
	quakeclass=Quakeclass.new
	it 'deve inicializar as variáveis' do
		expect(quakeclass.inicializavariaves).to be true 
		# puts "variáveis inicializadas com sucesso"
	end
	it 'deve exibir geraresultado preenchido' do
		expect(quakeclass.geraresultado("lib/qgames.log")).to_not be_nil  #.to have_type(:game_count)
		# puts "geraresultado não nulo"
	end

	it "iniciou game" do
		File.foreach("lib/qgames.log") do |line|
			expect(quakeclass.iniciouogame(line)).to be true  if line.include? "InitGame"
		end
	end

	it "processa kill" do
		File.foreach("lib/qgames.log") do |line|
			expect(quakeclass.fprocessakill(line)).to be true  if line.include? "Kill"
		end
	end

	it "deve verificar json gerado" do
		expect(quakeclass.gerajson).to include("game_count")
		expect(quakeclass.gerajson).to include("games")
	end

	it "deve verificar se gera fim de jogo" do
		expect(quakeclass.gerafimdejogo).to be true  
	end
end


#rspec spec/quake_spec.rb   rodar o teste na pasta raiz