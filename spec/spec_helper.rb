# frozen_string_literal: true
require 'active_support/all'
require 'support/request_helpers'
require 'jsonapi/rspec'


PROJECT_ROOT = File.expand_path('..', __dir__)

Dir.glob(File.join(PROJECT_ROOT, 'lib', '*.rb')).each do |file|
 autoload File.basename(file, '.rb').camelize, file
end

RSpec.configure do |config|
    config.include Requests::JsonHelpers, :type => :controller

    config.include JSONAPI::RSpec

    # Support for documents with mixed string/symbol keys. Disabled by default.
    config.jsonapi_indifferent_hash = true    
end
