require 'json'

class Quakeclass
	def inicializavariaves
		@game_count = 0
		@vtotal_kills = 0
		@vplayers = []
		@vkills = []
		@vkills_by_means = []
		@vmeans = []
		@data = ''
		@retorno=[]

		return true
	end

	def iniciouogame(line)
		if line.include? "InitGame"
			@game_count += 1
			@vtotal_kills = 0
			@vplayers = []
			@vkills = []		

			return true
		else
			return false
		end	
	end

	def fprocessakill(line)
		@vtotal_kills=@vtotal_kills+1
		
		vquemmatou = line.split(":").last.split("killed").first.delete_prefix(" ").delete_suffix(" ")
		vquemmorreu = line.split(":").last.split("killed").last.split("by").first.delete_prefix(" ").delete_suffix(" ")
		if !@vplayers.include? vquemmatou
			@vplayers << vquemmatou 
			@vkills << 0
		end

		if !@vplayers.include? vquemmorreu
			@vplayers << vquemmorreu 
			@vkills << 0
		end
		@vkills[@vplayers.index(vquemmatou)]+=1
		if vquemmatou == "<world>"
			@vkills[@vplayers.index(vquemmorreu)]-=1
		end

		vtipomorte = line.split("by ").last.gsub(" ", "").delete_suffix("\n")
		if !@vkills_by_means.include? vtipomorte
			@vkills_by_means << vtipomorte
			@vmeans << 0
		end
		@vmeans[@vkills_by_means.index(vtipomorte)] += 1

		return true
	end


	def gerajson
		vdata='{"game_count":'+@retorno.size.to_json+',"games":['+@retorno.join(',')+']}'
		vdatajson=JSON.parse vdata

		return vdatajson
	end

	def gerafimdejogo

		vplayersjson=""
		@vplayers.each do |vp|
			if vp != "<world>" 
				vplayersjson+="," if vplayersjson!=""
				vplayersjson+='{'+vp.to_json+":"+@vkills[@vplayers.index(vp)].to_json+'}' 
			end
		end

		vkills_by_meansjson=""
		@vkills_by_means.each do |vk|
			vkills_by_meansjson+="," if vkills_by_meansjson!=""
			vkills_by_meansjson+='{'+vk.to_json+":"+@vmeans[@vkills_by_means.index(vk)].to_json+'}'
		end

		vjson='{"game_'+@game_count.to_json+'":{"total_kills":'+@vtotal_kills.to_json+',"players":'+(@vplayers-["<world>"]).to_json+',"kills":['+vplayersjson+'], "kills_by_means": ['+vkills_by_meansjson+']}}'

		@retorno<<vjson 
		if @retorno<<vjson
			return true
		else
			return false
		end
	end


	def geraresultado(log)
		inicializavariaves
		File.foreach(log) do |line|

			iniciouogame(line)

			if line.include? "Kill"

				fprocessakill(line)
			end

			if line.include? "ShutdownGame"
				gerafimdejogo
			end	
		end	

		gerajson

	end
	
end