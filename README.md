# quake
O projeto implementa as seguintes funcionalidades:
<dl>
	<dt>- Ler o arquivo de log</dt>
	<dt>- Coleta, agrupa e exibe:</dt>
	<dd>-   Os dados de cada partida</dd>
	<dd>-   Os dados dos jogadores de cada partida</dd>
	<dd>-   As mortes em cada partida</dd>
	<dd>-   O tipo de morte em cada partida</dd>
	<dt>- Testa as funcionalidades</dt>
</dl>

Para executar o projeto, acesse a pasta /.lib e execute:	`ruby quake.rb`

Para executar o teste, execute na pasta raiz /quaketest:	`rspec spec/quake_spec.rb`

Versão do Ruby usada:	`ruby 2.7.2`

É necessário executar `bundle install` para rodar o projeto
